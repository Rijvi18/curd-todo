﻿using DIExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIExample.Infrastructure
{
    public interface IMeRepo
    {
        List<Me> Getall();
        Me AddItem(Me me);
        Me Edit(Me me);
        Me GetById(int id);
        Me Delete(int id);
    }
}
