﻿using DIExample.Infrastructure;
using DIExample.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DIExample.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMeRepo _repo;

        public HomeController(ILogger<HomeController> logger, IMeRepo Repo)
        {
            _logger = logger;
            _repo = Repo;
        }

        public IActionResult Index()
        {
            var items = _repo.Getall();
            return View(items);
        }
        //Get create
        public IActionResult Create() => View();
        //post create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Me me)
        {
            var item = _repo.AddItem(me);
            // return View(item);

            return RedirectToAction("Index");

        }
        //details

        public IActionResult Details(int id)
        {
            var item = _repo.GetById(id);
            return View(item);
        }

        //update
        public IActionResult Edit() => View();
        [HttpPut]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Me me)
        {
            var item = _repo.Edit(me);

            return RedirectToAction("Index");
        }
        public IActionResult Delete(int id)
        {
            var item = _repo.Delete(id);

            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
