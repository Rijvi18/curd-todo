﻿using DIExample.Infrastructure;
using DIExample.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIExample.Repository
{
    public class MeRepo : IMeRepo
    {
        private readonly aboutMeContext _context;
        public MeRepo(aboutMeContext context)
        {
            _context = context;
        }
        public List<Me> Getall()
        {
            return _context.Mes.ToList();
        }

        public Me AddItem(Me me)
        {
            if (me != null)
            {
                _context.Mes.Add(me);
                _context.SaveChanges();
                return me;
            }
            return null;
        }
        public Me Edit(Me me)
        {
           _context.Entry(me).State = EntityState.Modified;
            _context.SaveChanges();
            return me;
        }

        public Me GetById(int id)
        {
            return _context.Mes.FirstOrDefault(x => x.Id == id);
        }
        public Me Delete(int id)
            
        {
            var own=_context.Mes.FirstOrDefault(x => x.Id == id);
            _context.Entry(own).State =EntityState.Deleted;
            _context.SaveChanges();
            return own;
        }


    }
}
